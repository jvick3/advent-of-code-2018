(ns advent-of-code-2018.day5
 (:require [clojure.string :as s]))


(defonce polymer (slurp "resources/day5_input.txt"))

(def reaction-regex
 #"Aa|Bb|Cc|Dd|Ee|Ff|Gg|Hh|Ii|Jj|Kk|Ll|Mm|Nn|Oo|Pp|Qq|Rr|Ss|Tt|Uu|Vv|Ww|Xx|Yy|Zz|aA|bB|cC|dD|eE|fF|gG|hH|iI|jJ|kK|lL|mM|nN|oO|pP|qQ|rR|sS|tT|uU|vV|wW|xX|yY|zZ")


(defn react
 [poly-string]
 (loop [p poly-string]
   (let [p' (s/replace p reaction-regex "")]
     (if (= (count p) (count p'))
       p'
       (recur p')))))


(defonce letters
 (map (comp str char)
  (range (int \a) (inc (int \z)))))

(def reacted-polymer (react polymer))

; solution to puzzle one
(count reacted-polymer) ; => 10888

; solution to puzzle two: 6952
(comment
 (loop [smallest (count polymer)]
       units letters
  (if (empty? units)
    smallest
    (let [unit (first units)
          without-unit (-> polymer
                         (.replaceAll unit "")
                         (.replaceAll (.toUpperCase unit) ""))
          reacted-without-unit (react without-unit)
          new-length (count reacted-without-unit)]
     (println "unit" unit "went from length" (count polymer) "to" new-length)
     (recur
      (min smallest new-length)
      (rest units))))))
