(ns advent-of-code-2018.day2
 (:require [clojure.string :as str]))

(def lines
 (str/split-lines
  (slurp "resources/day2_input.txt")))


(defn letter-counts
 "Given a string, returns a sequence of the
 unique counts of letters in a string.  For example,
 a return of (1 3 4) means a letter appeared once
 (always does for non-empty string), at least a particular letter
 (but possibly multiple) appeared 3 times,
 and at least a particular letter (but possibly multiple) appeared 4 times."
 [s]
 (->> s
  (frequencies)
  (vals)
  (distinct)))


(defn score-vector
 "Given a string, returns a two-element vector [x y]
 where x is 1 if any letter appeared exactly twice in the string and 0 otherwise,
 and similarly for y but with three times instead of twice."
 [s]
 (let [counts (letter-counts s)]
  [(count (filter #{2} counts))
   (count (filter #{3} counts))]))


; Solution to puzzle one
(def checksum
 (->> (map score-vector lines) ; get score vectors (e.g. ([1 0] [0 1]))
      (apply mapv +)           ; apply sum over those vectors => tuple [x y]
      (apply *)))              ; multiple the tuple to get the checksum


; Solution for puzzle two

(defn equal-letters
 "Returns a String of the letters in common between two strings"
 [s1 s2]
 (let [letter-pairs (map vector s1 s2)
       letters-equal #(= (first %) (second %))]
      (apply str (map first (filter letters-equal letter-pairs)))))


(def solve-2
 ; sort lines to make it possible to compare sequential entries
 (let [sorted-ids (sort lines)]
  (loop [i 0]
   ; Look at each pair of words in sorted order
   (let [w1 (nth sorted-ids i)
         w2 (nth sorted-ids (inc i))
         ; get the letters in common
         el (equal-letters w1 w2)]
    (if (= (count el) (dec (count w1)))
     el  ; if all but one letter are common, return those equal letters
     (if (< i (dec (count sorted-ids))) ; otherwise keep searching
      (recur (inc i))))))))
