(ns advent-of-code-2018.day5
 (:require [clojure.string :as s]))


(defn manhattan-dist
 [[x1 y1] [x2 y2]]
 (+ (Math/abs (- x1 x2)) (Math/abs (- y1 y2))))


(defn closest
 "Returns the coordinate that is closest to the given loc.
     If there are multiple closest (equidistant), returns nil."
  [loc coords]
  (let [coords-by-dist (group-by (partial manhattan-dist loc) coords)
        nearest (coords-by-dist (apply min (keys coords-by-dist)))]
    (if (< (count nearest) 2)
      (first nearest)
      nil)))


(defn locations
 "Returns a sequence of locations (x/y tuples)
  that span the grid within the given coordinates
  (across the min/max x and y of the coordinates)"
 [coords]
 (let [coord-xs (map first coords)
       coord-ys (map second coords)
       xs (range
            (apply min coord-xs)
            (inc (apply max coord-xs)))
       ys (range
            (apply min coord-ys)
            (inc (apply max coord-ys)))]
   (for [x xs y ys] [x y])))


(defn closest-locs-by-coord
 "Returns a map from finite coordinate (those with finite area)
    to the locations that are closest to that coordinate."
 [coords]
 (-> (group-by #(closest % coords) (locations coords))
   ; remove nil keys, which represent locs which have multiple closest coordinates.
   (dissoc nil)))


(defn area-infinite?
 "Returns true if the area of the coordinate is infinite."
 [[x y] other-coords]
 ; TODO: can this be simplified or at least made less verbose?
 (let [points-left-and-up
         (filter #(and (< (first %) x) (< (second %) y)) other-coords)
       points-left-and-down
         (filter #(and (< (first %) x) (> (second %) y)) other-coords)
       points-right-and-up
         (filter #(and (> (first %) x) (< (second %) y)) other-coords)
       points-right-and-down
         (filter #(and (> (first %) x) (> (second %) y)) other-coords)
       xs (map first other-coords)
       ys (map second other-coords)]
       ; TODO: try and use seq here to check emptiness (idiomatic)
    (or (empty? points-left-and-up)
        (empty? points-left-and-down)
        (empty? points-right-and-up)
        (empty? points-right-and-down)
        (> x (apply max xs))
        (< x (apply min xs))
        (> y (apply max ys))
        (< y (apply min ys)))))



(defn only-infinite
 "Returns the coordinates that have infinite area."
 [coords]
 (filter
   #(area-infinite? % (remove #{%} coords))
   coords))


(defn solve-1
 [coords]
 (let [m (closest-locs-by-coord coords)] ; get closest locs by all coords
      ; filter out entries with infinite coordinates
   (->> (apply dissoc m (-> m keys only-infinite))
     vals  ; get the closest locs to each coordinate
     (map count) ; compute the area
     (apply max))))  ; get the maximum area


(defn parse-coords-line
 [line]
 (map #(Integer/parseInt (.trim %)) (s/split line #",")))


(defn read-coords-file
 [path]
 (map parse-coords-line (-> path slurp s/split-lines)))


(def test-input (read-coords-file "resources/day6_test.txt"))
(def input (read-coords-file "resources/day6_input.txt"))

 ; solution to puzzle one: 4398
(comment (solve-1 input))

; part two

(defn total-distance-to-coords
 "Returns the sum of the manhattan distances from the given loc
 to each of the coords"
 [loc coords]
 (reduce + (map (partial manhattan-dist loc) coords)))


(defn close-enough-locs
 "Returns the locations within the grid of coords that have
  total-distance-to-coords less than the specified max-dist" 
 [coords max-dist]
 (filter
   #(< (total-distance-to-coords % coords) max-dist)
   (locations coords)))


; answer to puzzle two: 39560
(comment (count (close-enough-locs input 10000)))
