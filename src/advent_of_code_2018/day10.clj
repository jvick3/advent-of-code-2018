(ns advent-of-code-2018.day10
 (:require [clojure.string :as str]))

; position=<-3,  6> velocity=< 2, -1>
; => {:x -3 :y 6 :x-vel 2 :y-vel -1}

(defn read-input-line
 [s]
 (zipmap [:x :y :x-vel :y-vel]
  (map #(Integer/parseInt %) (re-seq #"-*\d" s))))


(defn read-input
 [path]
 (map read-input-line (str/split-lines (slurp path))))


(def test-input (read-input "resources/day10_test.txt"))

(def input (read-input "resources/day10_input.txt"))


(defn point-move
 [point]
 (-> point
   (assoc :x (+ (:x point) (:x-vel point)))
   (assoc :y (+ (:y point) (:y-vel point)))))

; (map point-move test-input)

; how to display points?  GUI? 
