(ns advent-of-code-2018.day3-group
 (:require [clojure.string :as s]
           [clojure.java.io :as io]))

(defonce test-input
  (-> (io/resource "day3_test.txt")
      io/reader
      line-seq))

(def line-1 (first test-input))

(defn parse-id [s]
  (let [match (second (re-find #"#([0-9]*)" s))]
    {:id (Integer/parseInt match)}))

(defn parse-origin [s]
  (let [[x y] (rest (re-find #"([0-9]+),([0-9]+)" s))]
    {:x (Integer/parseInt x)
     :y (Integer/parseInt y)}))

(defn parse-dims [s]
  (let [[width height] (rest (re-find #"([0-9]+)x([0-9]+)" s))]
    {:width (Integer/parseInt width)
     :height (Integer/parseInt height)}))

(defn parse-claim [s]
  (merge
    (parse-id s)
    (parse-origin s)
    (parse-dims s)))

(parse-claim line-1)

(comment
 {:id 123
  :origin {:x 1 :y 3}
  :width 4
  :height 4})
