(ns advent-of-code-2018.day3
 (:require [clojure.string :as s]))


(defn parse-claim
 "Parse a claim string into it's components"
 [c]
 ; split on space to get each part of the claim
 (let [elems (s/split c #" ")
       ; split on comma to get individual coordinates
       coords (s/split (elems 2) #",")
       ; split on 'x' to get individual dimensions
       dims (s/split (elems 3) #"x")]
  ; return map of info
  {:id (Integer/parseInt (.substring (elems 0) 1))
   :x (Integer/parseInt (coords 0))
   :y (Integer/parseInt (.replace (coords 1) ":" ""))
   :width (Integer/parseInt (first dims))
   :height (Integer/parseInt (second dims))}))


(defn coords
 "Returns coordinates of a claim. Example:
 (coords {:x 1 :y 2 :width 2 :height 1) => [[1 2] [2 2]]"
 ([{:keys [x y width height]}]
  (vec
    (for
     ; add one to end of range to make inclusive
      [x' (range x (+ x width))
       y' (range y (+ y height))]
      [x' y']))))


(defn inc-int-val!
 [arr coord-pair]
 (let [x (first coord-pair)
       y (second coord-pair)
       val (aget arr x y)
       new-val (int (inc val))]
   (aset-int arr x y new-val)))


(defn compute-claims
 "Returns a 2D int vector of the claims, where each int represents
 the number of elves claiming that spot in the fabric."
 [claims]
 (let [claim-vecs (vec (repeat 1000 (vec (repeat 1000 0))))
       claim-board (into-array (map int-array claim-vecs))
       claim-coords (mapcat coords claims)]
    ; repeatedly increment the board over the cartesian coordinates
    ; described by the claim, returning the final board
    ; dorun is required because map returns a lazy sequence.
    (dorun (map (partial inc-int-val! claim-board) claim-coords))
    ; return the claim-board
    claim-board))


(defonce claims (map parse-claim (s/split-lines (slurp "resources/day3_input.txt"))))

(defonce applied-claims (compute-claims claims))

; Answer to puzzle one
(def overlapping-claims
 (count (filter #(> % 1) (reduce into [] applied-claims))))


(defn no-overlap?
 [claims c]
 (every? #(< % 2) (map #(aget claims (first %) (second %)) (coords c))))

; Answer to puzzle two
(def only-good-claim (filter (partial no-overlap? applied-claims) claims))
