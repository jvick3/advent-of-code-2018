(ns advent-of-code-2018.day4
 (:require [clojure.string :as s]))


(defn parse-minute
 [entry]
 (Integer/parseInt (.replace (re-find #"\d+\]" entry) "]" "")))


(defn parse-guard-id
 [entry]
 (Integer/parseInt (s/replace (re-find #"#\d+" entry) "#" "")))


(defn parse-log-entry
 [entry]
 (-> {:minute (parse-minute entry)}
  ; only put guard-id if it is present
  (cond-> (s/includes? entry "begin") (assoc :guard-id (parse-guard-id entry)))))


(defn multimap-add
 "If key is present in m, adds the val to the values of that key.
 If key is not present, adds the val as the first of the values of the key (new collection)"
 [m key val]
 (if (contains? m key)
   (assoc m key (conj (m key) val))
   (assoc m key [val])))



(defn guard-sleep-time-ranges
 "Returns a map from :guard-id to a vector of vectors,
 each vector containing an even number of numbers (minutes)
 of when the guard fell asleep and then woke up on a particular shift."
 [log-entries]
 (loop [guard-ranges {}
        entries log-entries]
       (if (empty? entries)
         guard-ranges
         (let [id (:guard-id (first entries))
               [sleep-wake-entries remaining-entries] (split-with #(nil? (:guard-id %)) (rest entries))
               sleep-wake-times (vec (map :minute sleep-wake-entries))]
           (recur
            (multimap-add guard-ranges id sleep-wake-times) remaining-entries)))))


(defn entry-with-max-val
 "Finds the map entry which has the maximum value in m."
 [m]
 (first (reverse (sort-by val m))))


(defn mode
 "Finds most commonly occuring number in data."
 [data]
 (first (last (sort-by second (frequencies data)))))


(defn ranges-to-nums
 "Returns a flat sequence of numbers that are between the nums.
 Example: (ranges-to-nums [ [1 4 6 8] [2 5]]) => (1 2 3 7 2 3 4)"
 [ranges]
 (reduce
  (fn [acc val] (concat acc (flatten (map #(range (first %) (second %)) (partition 2 val)))))
  [] ranges))


(defonce sorted-lines
 (sort (s/split-lines (slurp "resources/day4_input.txt"))))

(def log-entries (map parse-log-entry sorted-lines))

(def sleep-minutes-by-guard
 (reduce-kv
   (fn [m k v]
     (assoc m k (ranges-to-nums v)))
   {} (guard-sleep-time-ranges log-entries)))


(def sleepiest-guard
 (key
  (entry-with-max-val
    (reduce-kv
      (fn [m k v]
       (assoc m k (apply + v)))
      {} sleep-minutes-by-guard))))

(def sleepiest-minute
  (mode (get sleep-minutes-by-guard sleepiest-guard)))

(def puzzle-1 (* sleepiest-guard sleepiest-minute)) ; => 99911


; makes a map from guard-id to frequencies map where keys are minute
; and vals are how many time sthe guard slept on that minute
(def minute-freqs-by-guard
 (reduce-kv
   (fn [m k v] (assoc m k (frequencies v)))
   {} sleep-minutes-by-guard))

; finds the guard the slept the most on a particular minute,
; sets to a vector [guard-id [minute freq]]
(def guard-id-and-minute-and-freq
  (first
    (reverse
      (sort-by #(second (val %))
       (reduce-kv
         (fn [m k v] (assoc m k (entry-with-max-val v)))
         {} minute-freqs-by-guard)))))


; multipy the guard-id by the minute they sleep the most often.
(def puzzle-2  ; => 65854
 (* (first guard-id-and-minute-and-freq)
    (first (second guard-id-and-minute-and-freq))))
