(ns advent-of-code-2018.day7
 (:require [clojure.set :as st]))


(defn add-to-vals
  "If key is present in m, adds the val to the values of that key.
  If key is not present, adds the val as the first of the values of the key (new collection)"
  [m key val]
  (if (contains? m key)
    (assoc m key (conj (m key) val))
    (assoc m key (sorted-set val))))


(defn graph-entry
 [s]
 (let [elems (.split s " ")]
   (first {(.trim (get elems 1)) (.trim (get elems 7))})))

(defn read-dag
 [lines]
 (reduce
  (fn [m v] (add-to-vals m (key v) (val v)))
  {}
  (map graph-entry lines)))

(defn roots
 "Finds the roots of a graph (adjacency list) - nodes with no incoming edges."
 [graph]
 (filter
   (complement (apply st/union (vals graph)))
   (keys graph)))


(defn dependency-walk
 "Uses Kahn's algorithm to walk dependency graph
 and return vector of visited nodes."
 [graph]
 (loop [g graph
        available-steps (roots g)
        visited-steps []]
   (if (empty? available-steps) visited-steps
     (let [sorted-steps (sort available-steps)
           step (first sorted-steps)
           next-graph (dissoc g step)
           ; find any elem in (g step) that are not in (vals (dissoc g step))
           newly-avail-steps
           (filter (complement (apply st/union (vals next-graph))) (g step))]
       (recur
         next-graph
         (set (concat newly-avail-steps (rest sorted-steps)))
         (conj visited-steps step))))))

(def dag
 (-> "resources/day7_input.txt"
   slurp clojure.string/split-lines read-dag))


; Correct answer to puzzle one is GJKLDFNPTMQXIYHUVREOZSAWCB
(comment (apply str (dependency-walk dag)))

; puzzle 2 below

(defn time-required
 [step-str base-step-time]
 (+ base-step-time (- (int (first step-str)) 64)))


(defn worker-assignments
 [next-steps base-step-time]
 (map (fn [s] {:step s :remaining (time-required s base-step-time)}) next-steps))


(defn worker-done?
 [worker]
 (zero? (:remaining worker)))


(defn decrement-worker-times
 [workers]
 (map #(update % :remaining dec) workers))


(defn steps-not-being-worked
 [graph workers]
 (filter (complement (set (map :step workers))) graph))


(defn parallel-dependency-walk
 [graph num-workers base-step-time]
 (loop [sec 0
        g graph
        workers []
        available-steps (roots g)
        visited-steps []]
    (if (and (empty? g)
             (empty? (filter (complement worker-done?) workers)))
        {:seconds sec :steps visited-steps}
     (let [{active-workers false finished-workers true} (group-by worker-done? workers)
           completed-steps (map :step finished-workers)
           next-graph (apply dissoc g completed-steps)
           newly-avail-steps
           (filter (complement (apply st/union (vals next-graph)))
                   (apply st/union (map g completed-steps)))
           next-steps (take (- num-workers (count active-workers))
                        (sort (set (concat available-steps newly-avail-steps))))
           all-workers (concat active-workers (worker-assignments next-steps base-step-time))
           updated-workers (decrement-worker-times all-workers)]
         (recur
           (inc sec)
           next-graph
           updated-workers
           (steps-not-being-worked (roots next-graph) updated-workers)
           (concat visited-steps completed-steps))))))

; Solution to puzzle 2
(comment (:seconds (parallel-dependency-walk dag 5 60)))
