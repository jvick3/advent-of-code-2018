(ns advent-of-code-2018.day1
  (:require [clojure.string :as str]))


(def lines (str/split-lines (slurp "resources/day1_input.txt")))

(def freqs (map #(Integer/parseInt %) lines))

; Answer to puzzle one
(comment (reduce + freqs))

(defn first-duplicate [elems]
  (let [conj-or-find (fn [seen e]
                       (if (seen e)
                         (reduced e)
                         (conj seen e)))
        result (reduce conj-or-find #{} elems)]
    (if (set? result) nil result)))

; Reason for cycling freqs:
; "Note that your device might need to repeat its list of frequency changes
; many times before a duplicate frequency is found"
(def freq-sums (cons 0 (reductions + (cycle freqs))))

; Answer to puzzle two
(comment (first-duplicate freq-sums))
