(ns advent-of-code-2018.day8)


(defn read-input
 [s]
 (map #(Integer/parseInt (clojure.string/trim-newline %))
   (clojure.string/split (slurp s) #" ")))


(def test-input (read-input "resources/day8_test.txt"))

(def input (read-input "resources/day8_input.txt"))


; Modeled after: https://github.com/borkdude/advent-of-cljc/blob/master/src/aoc/y2018/d08/borkdude.cljc#L20
(defn parse-tree
 [[num-children num-meta & nodes]]
 (loop [children-remaining num-children
        nodes nodes
        children []]
   (if (zero? children-remaining)
     (let [[metas remainder] (split-at num-meta nodes)]
       [{:metas (vec metas)
         :children children} remainder])
     (let [[new-child new-child-nodes] (parse-tree nodes)]
       (recur (dec children-remaining) new-child-nodes (conj children new-child))))))


(defn metas
 [[root & rest]]
 (if (seq root)
   (concat (:metas root)
           (metas (:children root))
           (metas rest))
   []))


; Answer for part 1: 49426
; (solve-1 input)
(defn solve-1
 [nodes]
 (reduce +
  (metas (parse-tree nodes))))


(defn node-value
 [node]
 (if-let [children (seq (:children node))]
   (reduce + (map #(node-value (get (vec children) (dec %))) (:metas node)))
   (reduce + (:metas node))))


; Answer for part 2: 40688
; (solve-2 input)
(defn solve-2
 [nodes]
 (node-value (first (parse-tree nodes))))
